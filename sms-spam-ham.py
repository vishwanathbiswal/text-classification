# -*- coding: utf-8 -*-
"""
Created on Mon Jul  2 11:20:25 2018

@author: Vishwanath Biswal
"""

# Importing Libraries
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


# Reading given file in json format
df = pd.read_json('spam_ham.json')

# coverting json data into csv
df.to_csv('data.csv', index=False, columns=['v1', 'v2'])

# Reading csv dataset
dataset = pd.read_csv('data.csv')

# Renaming v1 to Label & v2 to SMS
dataset = dataset.rename(index=str , columns={'v1':'label','v2':'messages'})

# Labeling ( Ham as 0 ) and ( spam as 1 )
dataset['label'] = dataset.label.map({'ham':0, 'spam':1})


# Visualizing whether dataset is empty or not
sns.heatmap(dataset.isnull(), yticklabels=False, cmap='viridis')

# Visualizing number of spam SMS and ham SMS
sns.countplot(x="label", data=dataset)

# Viewing aggregate statistics using pandas:
dataset.groupby('label').describe()

# How long are the messages?
# Printing length of each messages .......
dataset['length'] = dataset['messages'].map(lambda text: len(text))
print(dataset.head())

# Using matplotlib, visualizing length of each messages
dataset.length.plot(bins=20, kind='hist')

# Printing descriptions such as count(total_no._of_messages), min_length, max_length etc.
dataset.length.describe()

# Printing super long message
print(list(dataset.messages[dataset.length > 900]))

# Visualizing difference in message length between spam and ham
dataset.hist(column='length', by='label', bins=50)


# A collection of texts is also sometimes called "corpus".
# Therefore, column SMS is said to be corpus
'''This corpus will be our labeled training set. Using these ham/spam examples, we will 
train a machine learning model to learn to discriminate between ham/spam automatically. 
Then, with a trained model, we will be able to classify arbitrary unlabeled messages 
as ham or spam.'''


# Cleaning the texts
import re
import nltk
#nltk.download()
#nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer


corpus = []
for i in range(0, 5572):
    msg = re.sub('[^a-zA-Z]', ' ', dataset['messages'][i])
    msg = msg.lower()
    msg = msg.split()
    ps = PorterStemmer()
    msg = [ps.stem(word) for word in msg if not word in set(stopwords.words('english'))]
    msg = ' '.join(msg)
    corpus.append(msg)
    

        
# Creating the Bag of Words model
from sklearn.feature_extraction.text import CountVectorizer
#cv = CountVectorizer(max_features = 2500)

cv = CountVectorizer()  # set the variable
cv.fit(corpus)  # fit the function 
cv.get_feature_names()  # get the outputs

   
features = cv.fit_transform(corpus).toarray()
labels = dataset.iloc[:, 0:1]



# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
features_train, features_test, labels_train, labels_test = train_test_split(features, labels, test_size = 0.20, random_state = 0)
    


'''Applying Gaussian Naive-bayes'''

## Fitting Naive Bayes to the Training set
#from sklearn.naive_bayes import GaussianNB
#classifier = GaussianNB()
#classifier.fit(features_train, labels_train)
#    
## Predicting the Test set results
#labels_pred = classifier.predict(features_test)
#    
#score_using_gaussian = classifier.score (features_test , labels_test) # 0.864573991031
#
## Making the Confusion Matrix
#from sklearn.metrics import confusion_matrix
#cm_using_gaussian = confusion_matrix(labels_test, labels_pred)
    
    
''' Applying Multinomial Naive-bayes'''

# Fitting Naive Bayes to the Training set
from sklearn.naive_bayes import MultinomialNB
mlt_nb = MultinomialNB()
mlt_nb.fit(features_train , labels_train)

# Predicting the Test set results
mulNB_predict = mlt_nb.predict(features_test)

# Checking score
score_using_multinomial = mlt_nb.score(features_test , labels_test)  # 0.979372197309

# Determining confusion matrix    
from sklearn.metrics import confusion_matrix
cm_for_MultinomialNB = confusion_matrix(labels_test, mulNB_predict)


'''Applying Bernoulli Naive-bayes'''    

## Applying Bernoulli Naive-bayes
#from sklearn.naive_bayes import BernoulliNB
#ber_NB = BernoulliNB()
#ber_NB.fit(features_train , labels_train)
#berNB_predict  = ber_NB.predict(features_test)
#    
#    
#score_using_Bernoulli = ber_NB.score(features_test , labels_test)  # 0.967713004484
#
#from sklearn.metrics import confusion_matrix
#cm_for_bernoulli = confusion_matrix(labels_test , berNB_predict)
#


''' We'd applied Multinomial Naive-bayes , out of these three. This is because Multinomial
Naive-bayes gives better score out of all the Naive-bayes algorithms'''


# Now user predicts message whether it's a spam or ham by entering some inputs
    
test_str = "FreeMsg Hey there darling it's been 3 week's now and no word back! I'd like some fun you up for it still? Tb ok! XxX std chgs to send, ï¿½1.50 to rcv."
test_str_series = list()
test_str_series.append(test_str)
test_str_df = cv.transform(test_str_series).toarray()
    
predict_result = mlt_nb.predict(test_str_df)  # spam
    
    
    
#test_str1 = "Ok lar... Joking wif u oni..."
#test_str_series1 = list()
#test_str_series1.append(test_str1)
#test_str_df1 = cv.transform(test_str_series1).toarray()
#    
#predict_result1 = mlt_nb.predict(test_str_df1)  # ham
#    
#    
#    
#test_str2 = "FreeMsg Hey there darling it's been 3 week's now and no word back! I'd like some fun you up for it still? Tb ok! XxX std chgs to send, �1.50 to rcv"
#test_str_series2 = list()
#test_str_series2.append(test_str2)
#test_str_df2 = cv.transform(test_str_series2).toarray()
#    
#predict_result2 = mlt_nb.predict(test_str_df2)  # spam
#    
#
#    
#test_str3 = "As per your request 'Melle Melle (Oru Minnaminunginte Nurungu Vettam)' has been set as your callertune for all Callers. Press *9 to copy your friends Callertune"
#test_str_series3 = list()
#test_str_series3.append(test_str3)
#test_str_df3 = cv.transform(test_str_series3)
#
#predict_result3 = mlt_nb.predict(test_str_df3)  # ham
#
#
#
#test_str4 = "A [redacted] loan for £950 is approved for you if you receive this SMS. 1 min verification & cash in 1 hr at www.[redacted].co.uk to opt out reply stop"
#test_str_series4 = list()
#test_str_series4.append(test_str4)
#test_str_df4 = cv.transform(test_str_series4)
#
#predict_result4 = mlt_nb.predict(test_str_df4)  # spam




'''Word Cloud for Spam and Ham SMS'''

# Visualizing WordCloud for spam messages

from wordcloud import WordCloud


spam_words  = ' '.join(list(dataset[dataset['label']==1]['messages']))
spam_wc = WordCloud(width=512 , height=512).generate(spam_words)
plt.figure(figsize = (10,8) , facecolor = 'k')
plt.imshow(spam_wc)
plt.axis('off')
plt.tight_layout(pad = 0)
plt.show()


# Visualizing WordCloud for ham messages

ham_words  = ' '.join(list(dataset[dataset['label']==0]['messages']))
ham_wc = WordCloud(width=652,height=512).generate(ham_words)
plt.figure(figsize = (10,8) , facecolor = 'k')
plt.imshow(ham_wc)
plt.axis('off')
plt.tight_layout(pad = 0)
plt.show()


